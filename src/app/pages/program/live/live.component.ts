import * as _ from 'lodash';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { forkJoin as observableForkJoin, Observable } from 'rxjs';
import { AgendaService } from 'src/app/services/api/agenda.service';
import { DateService } from 'src/app/services/api/date.service';
import { RoomService } from 'src/app/services/api/room.service';
import { map } from 'rxjs/operators';
import { MemberService } from 'src/app/services/api/member.service';
import { HistoryService } from 'src/app/services/api/history.service';

declare var $: any;

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.scss']
})
export class LiveComponent implements OnInit, OnDestroy {
  @ViewChild('entryAlertBtn') entryAlertBtn: ElementRef;

  public user: any; // 사용자 정보
  public isLiveAgenda: any; // 현재 진행중인 아젠다

  public dates: any[] = []; // 날짜 목록
  public rooms: any[] = []; // 룸 목록

  public allAgendas: any[] = []; // 모든 강의
  public agendas: any[] = []; // 날짜/룸으로 필터링 된 강의.

  public selected: any = {
    date: null,
    room: null
  };

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public dateService: DateService,
    public roomService: RoomService,
    public agendaService: AgendaService,
    public memberService: MemberService,
    public historyService: HistoryService,
  ) {
    this.doInit();
  }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('cfair'));
  }

  doInit(): void {
    this.historyService.setAttendance('in');

    const observables = [this.getAgendas(), this.getDates(), this.getRooms()];
    observableForkJoin(observables)
      .pipe(
        map(resp => {
          this.allAgendas = resp[0];
          this.dates = resp[1];
          this.rooms = resp[2];
          return resp;
        })
      )
      .subscribe(res => {
        let date = this.dates[0];
        let room = this.rooms[0];
        this.route.queryParams.subscribe(param => {
          // 선택된 날짜/룸이 있다면 해당 날짜와 룸이 선택될 수 있도록 한다.
          if (param.dateId && param.roomId) {
            date = this.dates.find(date => { return date.id === param.dateId; });
            room = this.rooms.find(room => { return room.id === param.roomId; });
          }
          this.router.navigate([], { queryParams: { dateId: null, roomId: null }, queryParamsHandling: 'merge' });
        });
        this.selected = { date: date, room: room }
        this.setAgendaList(date, room)
      });
  }

  public next: any;
  // Date/Room 선택 시 Agenda Filtering
  setAgendaList(date: any, room: any, next?: boolean) {
    if (!this.historyService.isAttended()) {
      // 입장
      this.entryAlertBtn.nativeElement.click();
      this.next = () => { this.setAgendaList(date, room, true) };
    } else {
      // 입장/퇴장
      if (next) this.next = null;
      let num = next ? 500 : 0;

      setTimeout(() => {
        this.selected = { date: date, room: room };
        this.agendas = this.allAgendas.filter(agenda => {
          return date.id === agenda.date.id && room.id === agenda.room.id;
        });

        setTimeout(() => {
          this.entryAlertBtn.nativeElement.click();
          this.checkLiveAgenda();
        }, 0);

      }, num)
    }
  }

  // 현재 진행중인 아젠다 확인
  checkLiveAgenda(): void {
    this.isLiveAgenda = this.agendas[0];
    this.agendas.forEach((agenda: any) => {
      const agendaDate = agenda.date.date.replace(/-/gi, '/');
      const agendaStartTime = new Date(`${agendaDate}/${agenda.startTime}`);
      const agendaEndTime = new Date(`${agendaDate}/${agenda.endTime}`);
      const now: Date = new Date();
      if (now > agendaStartTime && now < agendaEndTime) {
        this.isLiveAgenda = agenda;
      }
    });
  }

  /** 아젠다 목록 조회 */
  getAgendas(): Observable<any> {
    return this.agendaService.find();
  }

  /** 날짜 목록 조회 */
  getDates(): Observable<any> {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms(): Observable<any> {
    return this.roomService.find();
  }

  /**
   * 강의 상세보기
   * @param agenda 강의
   */
  goDetail(agenda: any): void {
    this.router.navigate([`/live/${this.selected.date.id}/${this.selected.room.id}/${agenda.id}`]);
  }

  // 출석체크
  atndnCheck(isMove?): void {
    const options: { relationId: string, relationType: string, logType: string } = {
      relationId: this.selected.room.id,
      relationType: 'room',
      logType: this.historyService.getAttendance()
    };

    localStorage.setItem('currentRoom', this.selected.room.id);

    this.memberService.history(this.user.id, options)
      .subscribe(
        (resp) => {
          if (this.historyService.isAttended()) {
            this.historyService.setAttendance('out');
          } else {
            this.historyService.setAttendance('in');
          }
          if (this.next) {
            this.next();
          }

          if (isMove) {
            this.entryAlertBtn.nativeElement.click();
          }
        },
        (error) => {
          console.error(error);
        });
  }

  // 참여 상태 출력
  getAttendance(): string {
    return this.historyService.isAttended() ? 'in' : 'out';
  }

  ngOnDestroy(): void { }

  @HostListener('window:beforeunload', ['$event'])
  public beforeunloadHandler($event) {
    if (!this.historyService.isAttended()) {
      this.atndnCheck(true);
    }

    $event.preventDefault();
    ($event || window.event).returnValue = '로그아웃 하시겠습니까?';
    return false;
  }

}
