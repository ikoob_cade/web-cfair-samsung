import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { _ParseAST } from '@angular/compiler';
// import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import '@devmobiliza/videojs-vimeo/dist/videojs-vimeo.esm';
import { MemberService } from 'src/app/services/api/member.service';
import * as _ from 'lodash';

declare let UstreamEmbed: any;

@Component({
  selector: 'app-player-ustream',
  templateUrl: './player-ustream.component.html',
  styleUrls: ['./player-ustream.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PlayerUstreamComponent implements OnInit, AfterViewInit {
  public user: any;
  public viewer: any;

  // TODO 임시데이타
  constructor(
    public fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private memberService: MemberService,
  ) {
  }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('cfair'));
    this.setUstreamViewer();
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
  }

  setUstreamViewer(): void {
    if (UstreamEmbed) {
      this.viewer = UstreamEmbed('UstreamIframe');
      // document.getElementById('UstreamIframe').style.minHeight = '-webkit-fill-available';

      this.viewer.addListener('live', (event, data) => {
        console.log(event + ' = ' + data);
      });

      this.viewer.addListener('finished', (event, data) => {
        console.log(event + ' = ' + data);
        this.viewer.getProperty('progress', function (progress) {
          console.log('progress = ' + progress);
        });
      });
      this.viewer.addListener('offline', (event, data) => {
        console.log(event + ' = ' + data);
        this.viewer.getProperty('progress', function (progress) {
          console.log('progress = ' + progress);
        });
      });
      this.viewer.addListener('playing', (event, data) => {
        console.log(event + ' = ' + data);
      });
    } else {
      console.log('no UstreamEmbed');
    }
  }
}
