import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { PosterService } from 'src/app/services/api/poster.service';

@Component({
  selector: 'app-posters-detail',
  templateUrl: './posters-detail.component.html',
  styleUrls: ['./posters-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class PostersDetailComponent implements OnInit {
  posterId: string;
  selectedPoster: any = [];

  constructor(
    public route: ActivatedRoute,
    private posterService: PosterService) {

    this.posterId = route.snapshot.params['posterId']
    this.posterService.findOne(this.posterId).subscribe(res => {
      this.selectedPoster = res;
      console.log('GET Poster', this.selectedPoster);
    });
  }

  ngOnInit(): void {

  }

}
