import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';


declare var $: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  @ViewChild('policyAlertBtn') policyAlertBtn: ElementRef;
  @ViewChild('passwordErrorMesage') passwordErrorMesage: ElementRef;

  private user: any; // 유저정보

  public passwordError = ' '; // 로그인 에러메세지

  // 로그인 폼
  public member = {
    email: '',
    password: '',
  };

  public pwdEmail = ''; // 패스워드 찾기의 이메일 input

  constructor(
    public router: Router,
    public auth: AuthService) { }

  ngOnInit(): void {
  }

  /** 
   * 사용자 로그인
   */
  login(): void {
    this.auth.login(this.member).subscribe(res => {
      if (res.token) {
        this.user = res;
        this.policyAlertBtn.nativeElement.click();
      }
    }, error => {
      if (error.status === 401) {
        this.passwordError = '로그인 정보가 일치하지 않습니다.';
      }
      if (error.status === 404) {
        this.passwordError = '로그인 정보를 찾을 수 없습니다.';
      }
    });
  }

  /**
   * 메인페이지로 이동
   */
  goToMain(): void {
    localStorage.setItem('cfair', JSON.stringify(this.user));
    this.router.navigate(['/main']);
  }

  /**
   * 초기화 이메일을 전송한다.
   */
  sendEmail(): void {
    if (this.pwdEmail) {
      this.auth.sendResetEmail(this.pwdEmail).subscribe(res => {
        if (res) {
          alert('해당 이메일로 임시 비밀번호가 전송되었습니다.');
          this.cancelFindPwd();
        }
      });
    } else {
      alert('이메일을 입력해 주세요.');
    }
  }

  /**
   * 비밀번호 찾기 닫기
   */
  cancelFindPwd = () => {
    $('#findPassword').modal('hide');
    this.pwdEmail = '';
  }

}
