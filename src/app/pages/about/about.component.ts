import { Component, OnInit, Input } from '@angular/core';
import { EventService } from 'src/app/services/api/event.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
  @Input('opt') opt: boolean = false;

  public event;
  // public description: string;

  constructor(private eventService: EventService ) { }

  ngOnInit(): void {
    this.loadEventInfo();
  }

  loadEventInfo = () => {
    this.eventService.findOne().subscribe(res => {
      this.event = res;
    });
  }

}
