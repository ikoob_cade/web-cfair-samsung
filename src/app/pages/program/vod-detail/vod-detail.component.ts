import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { VodService } from 'src/app/services/api/vod.service';

@Component({
  selector: 'app-vod-detail',
  templateUrl: './vod-detail.component.html',
  styleUrls: ['./vod-detail.component.scss']
})
export class VodDetailComponent implements OnInit {

  public vod: any;

  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    public vodService: VodService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      if (params.vodId) {
        this.vodService.findOne(params.vodId).subscribe(vod => {
          this.vod = vod.contents;
        });
      }
    });
  }

  goBack(): void {
    this.location.back();
  }


}
