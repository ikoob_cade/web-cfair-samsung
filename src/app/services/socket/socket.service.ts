import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment'
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private url = 'https://cfair-socket-ssl.cf-air.com';
  private socket;

  constructor() {
    this.socket = io(this.url, {
      transports: ['websocket']
    });
    console.log(this.socket);

    this.socket.on('connect', () => {
      const user = JSON.parse(localStorage.getItem('cfair'));
      if (user) {
        this.login({ memberId: user.id });
      }
      console.log('connected');
    });

    this.socket.on('disconnect', () => {
      console.log('disconnected');
    });
  }

  login(data) {
    this.socket.emit('login', data, (result) => {
      console.log(result);
    });
  }

  logout(data) {
    this.socket.emit('logout', data, (result) => {
      console.log(result);
    });
  }

}
