import { Component, OnInit } from '@angular/core';
import { VodService } from 'src/app/services/api/vod.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-program',
  templateUrl: './program.component.html',
  styleUrls: ['./program.component.scss']
})
export class ProgramComponent implements OnInit {
  public vods: any[] = [];

  constructor(
    private router: Router,
    private vodService: VodService
  ) {
    this.getVod();
  }

  ngOnInit(): void {
  }

  public getVod(): void {
    this.vodService.find().subscribe((response: any) => {
      this.vods = response;
    });
  }

  public goDetail(vod:any): void {
    this.router.navigate([`/vod/${vod.id}`]);
  }

}
