import { Component, OnInit } from '@angular/core';
import { VodService } from 'src/app/services/api/vod.service';

@Component({
  selector: 'app-vod',
  templateUrl: './vod.component.html',
  styleUrls: ['./vod.component.scss']
})
export class VodComponent implements OnInit {

  public vods: Array<any>;

  constructor(
    private vodService: VodService,
  ) { }

  ngOnInit(): void {
    this.loadVods();
  }

  loadVods = () => {
    this.vodService.find().subscribe(res => {
      console.log('GET Vods', res);
      this.vods = res;
    });
  }
}
