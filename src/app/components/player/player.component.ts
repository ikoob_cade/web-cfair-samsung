import { Component, OnInit, Input, OnChanges, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { _ParseAST } from '@angular/compiler';
// import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import '@devmobiliza/videojs-vimeo/dist/videojs-vimeo.esm';
import { MemberService } from 'src/app/services/api/member.service';
import videojs from 'video.js';
import * as _ from 'lodash';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit, AfterViewInit {
  @ViewChild('videoPlayer') videoPlayer: ElementRef;
  @Input('content') content: any; // 전달받은 콘텐츠 정보

  public user: any;
  public videoPlayerObject;
  public reg = /vimeo.com/;
  public player: any;

  // TODO 임시데이타
  public activeSponsor: any = '1';
  public replyForm: FormGroup;
  public replys: any = [
    { username: 'Seung-Hwan Lee', content: '강의 잘 들었습니다.' },
  ];

  constructor(
    public fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private memberService: MemberService,
  ) {
    this.replyForm = fb.group({
      username: ['tester'],
      content: ['', Validators.compose([Validators.required, Validators.minLength(10)])]
    });
  }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('cfair'));
    // this.getReplys();
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
    this.setVideoPlayer();
  }

  setVideoPlayer(): void {
    if (this.reg.test(this.content.contentUrl)) {  // Vimeo
      const opt: any = {
        controls: false,
        controlBar: {
          subsCapsButton: false,
          subtitlesButton: false,
          pictureInPictureToggle: false,
        },
        fluid: true,
        html5: { nativeTextTracks: false },
        preload: 'auto',
        thumbnail: this.content.thumbNailUrl,
        sources: {
          src: this.content.contentUrl,
          type: 'video/vimeo'
        },
      };
      this.player = new videojs(this.videoPlayer.nativeElement, opt, () => {

        const that = this;
        this.player.on('error', (error) => {
          console.log('error');
          this.offVideoEvent();
        });

        // 동영상 재생 위치 변경 감지
        this.player.on('loadeddata', () => {
          console.log('loadeddata');
          that.player.off('loadeddata');
        });

        // 플레이어 초기 지속 시간 발생 감지
        this.player.on('loadedmetadata', () => {
          console.log('loadedmetadata');
          that.player.off('loadedmetadata');
        });

        // 동영상 종료
        this.player.on('ended', () => {
          console.log('ended');
          that.player.off('ended');
          that.player.initChildren();
        });

        // 동영상 재생
        this.player.on('play', () => {
          console.log('play');
        });

        // 동영상 일시정지
        // 기존에 일시정지하면 바로 서버에 로그를 기록했는데, 클라이언트에서 10초 간격으로 계산하여 서버에 요청하는 구조로 바뀜.
        // 이력은 그대로 냅둔다.
        this.player.on('pause', () => {
          console.log('pause');
        });
      });
    }
  }

  offVideoEvent = () => {
    if (this.player) {
      this.player.off('loaded');
      this.player.off('ended');
      this.player.off('play');
      this.player.off('pause');
      this.player.off('seeked');
      this.player.off('seekable');
      // this.player.off('timeupdate');
    }
  }

  /** 댓글 불러오기 */
  getReplys(): void {
    this.memberService.findComment(this.user.id).subscribe(res => {
      this.replys = res;
    });
  }

  /** 댓글달기 */
  reply(): void {
    // this.replys = [this.replyForm.value].concat(...this.replys);
    // this.replyForm.patchValue({
    //   content: ''
    // });
    return console.log(this.replyForm.value.content);
    this.memberService.createComment({
      memberId: this.user.id,
      title: '',
      content: this.replyForm.value.content
    }).subscribe(res => this.getReplys());
  }
}
