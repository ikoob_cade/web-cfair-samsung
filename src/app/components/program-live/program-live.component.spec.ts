import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramLiveComponent } from './program-live.component';

describe('AgendaVideoComponent', () => {
  let component: ProgramLiveComponent;
  let fixture: ComponentFixture<ProgramLiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramLiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramLiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
