import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import cssVars from 'css-vars-ponyfill';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

cssVars({
	include: 'style',
	onlyLegacy: false,
	watch: true,
	// onComplete(cssText, styleNode, cssVariables) {
	// 	console.log(cssText);
	// }
});

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
