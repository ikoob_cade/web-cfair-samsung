import { Component, OnInit, HostListener } from '@angular/core';
import { SpeakerService } from 'src/app/services/api/speaker.service';
import * as _ from 'lodash';
import { _ParseAST } from '@angular/compiler';
import { SponsorService } from 'src/app/services/api/sponsor.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public ebookUrl = 'https://cfair-files.s3.ap-northeast-2.amazonaws.com/events/5f4c91452c527e7918232001/%EC%B4%88%EB%A1%9D%EC%A7%91-%EC%82%BC%EC%84%B1%EC%84%9C%EC%9A%B8%EB%B3%91%EC%9B%90_%EC%A0%9C7%ED%9A%8C_%EA%B5%AD%EC%A0%9C%EC%8B%AC%ED%8F%AC%EC%A7%80%EC%97%84.pdf';

  public speakers: Array<any> = [];
  public categories: Array<any> = []; // 카테고리[스폰서] 목록
  public mobile: boolean;

  constructor(
    private sponsorService: SponsorService,
    private speakerService: SpeakerService,
  ) { }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth < 768 !== this.mobile) {
      this.loadSpeakers();
    }
  }

  /**
   * 배열 나누기
   * @param array 배열
   * @param n n개씩 자르기
   */
  division = (array, n) => {
    return [].concat.apply([],
      array.map((item, i) => {
        return i % n ? [] : [array.slice(i, i + n)]
      })
    );
  }

  ngOnInit(): void {
    this.loadSpeakers();
    this.loadSponsors();
  }

  // 발표자 리스트를 조회한다.
  loadSpeakers(): void {
    let limit = 6;
    this.mobile = window.innerWidth < 768;
    if (this.mobile) {
      limit = 6;
    }
    this.speakerService.find(true).subscribe(res => {
      this.speakers = this.division(res.International, limit);
    });
  }

  /**
   * 스폰서 목록 조회
   */
  loadSponsors = () => {
    this.sponsorService.find().subscribe(res => {
      console.log('GET Sponsors', res);

      this.categories =
        _.chain(res)
          .groupBy(sponsor => {
            return sponsor.category ? JSON.stringify(sponsor.category) : '{}';
          })
          .map((sponsor, category) => {
            category = JSON.parse(category);
            category.sponsors = sponsor;
            return category;
          }).sortBy(category => {
            return category.seq;
          })
          .value();

      // console.log(this.categories);

    });
  }

}
