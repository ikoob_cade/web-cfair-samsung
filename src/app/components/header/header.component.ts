import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { MemberService } from 'src/app/services/api/member.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { HistoryService } from 'src/app/services/api/history.service';
import { SocketService } from 'src/app/services/socket/socket.service';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public user: any;
  public menus: any = [
    { title: 'Welcome', router: '/about' },
    { title: 'Speakers', router: '/speakers' },
    { title: 'Program', router: '/program' },
    { title: 'Live', router: '/live' },
    // { title: 'Posters', router: '/posters' },
    { title: 'Sponsors', router: '/sponsors' },
  ];

  collapse: boolean = true;

  constructor(
    private router: Router,
    private memberService: MemberService,
    private historyService: HistoryService,
    private authService: AuthService,
    private socketService: SocketService
  ) {
    router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((val) => {
        const auth = JSON.parse(localStorage.getItem('cfair'));
        this.user = (auth && auth.token) ? auth : undefined;

        if (this.user) {
          this.loginSocket()
        }

        $('.navbar-collapse').collapse('hide');
        this.collapse = true;
      });
  }

  ngOnInit(): void {

  }


  postAttendance = (): void => {
    this.memberService.attendance(this.user.id).subscribe(res => { });
  }

  /** 로그아웃 버튼. */
  logoutBtn(): void {
    if (this.router.url === '/live') {
      if (confirm('로그아웃 하시겠습니까?\n(현재 시간으로 퇴실 처리됩니다.)')) {
        const options: { relationId: string, relationType: string, logType: string } = {
          relationId: localStorage.getItem('currentRoom'),
          relationType: 'room',
          logType: this.historyService.getAttendance()
        };

        const user = JSON.parse(localStorage.getItem('cfair'));

        // 서버에 저장
        this.memberService.history(
          user.id,
          options).subscribe(res => {
            localStorage.removeItem('currentRoom');
            this.logout();
          });
        this.historyService.setAttendance(null);
      }
    } else {
      this.logout();
    }
  }

  /** 로그아웃 */
  logout(): void {
    this.authService.logout().subscribe(res => {
      this.logoutSocket();

      localStorage.removeItem('cfair');
      this.router.navigate(['/']);
    }, error => {
      console.error('Logout Error', error);
      alert('로그아웃에 실패했습니다.');
    });
  }


  loginSocket() {
    this.socketService.login({
      memberId: this.user.id
    });
  }

  logoutSocket() {
    this.socketService.logout({
      memberId: this.user.id
    });
  }
}
